from __future__ import print_function
from marshmallow import fields, ValidationError, fields
from sqlalchemy import inspect


class NaiveDateTime(fields.DateTime):

    def _deserialize(self, value):
        offset_aware = super(NaiveDateTime, self)._deserialize(value)
        return offset_aware.replace(tzinfo=None)


class Integer(fields.Int):

    def __init__(self, default=None, *args, **kwargs):
        super(Integer, self).__init__(default=default, *args, **kwargs)


class String(fields.Str):

    def __init__(self, default=None, *args, **kwargs):
        super(String, self).__init__(default=default, *args, **kwargs)


class Float(fields.Float):

    def __init__(self, default=None, *args, **kwargs):
        super(Float, self).__init__(default=default, *args, **kwargs)


class RelatedNest(fields.Nested):

    def __init__(self, nested, default=None, *args, **kwargs):
        if kwargs.get('many', None):
            default = []
        super(RelatedNest, self).__init__(nested, default=default, *args, **kwargs)

    @property
    def session(self):
        return self.parent.opts.sqla_session

    @property
    def model(self):
        return self.parent.opts.model

    @property
    def related_model(self):
        return getattr(self.model, self.attribute or self.name) \
            .property.mapper.class_

    @property
    def related_column(self):
        # if self.column:
        #     return self.related_model.__mapper__.columns[self.column]
        return inspect(self.related_model).primary_key[0]

    def get_pk(self, value):
        if isinstance(value, int):
            return value
        elif isinstance(value, dict):
            pk = value.get(self.related_column.name, None)
            if pk:
                return pk
            else:
                errors = self.schema.validate(value)
                if errors:
                    print('errors', errors)
                    raise ValidationError(errors)
                else:
                    data, errors = self.schema.load(value)
                    self.session.add(data)
                    self.session.commit()
                    return data.id

        return value

    def _deserialize(self, value):
        if not self.metadata.get('dump_only', False):
            if isinstance(value, list):
                value = [self.get_pk(v) for v in value]
                orm_return = self.session.query(self.related_model) \
                    .filter(self.related_column.in_(value)).all()
            else:
                value = self.get_pk(value)
                orm_return = self.session.query(self.related_model) \
                    .filter(self.related_column == value).one()
            return orm_return