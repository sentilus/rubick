from flask.ext.marshmallow.sqla import ModelSchema

from rubick.rest.fields import Integer


class SentiSchema(ModelSchema):

    id = Integer(put_only=True)

    def __init__(self, method=None, existing_object=None, *args, **kwargs):
        super(SentiSchema, self).__init__(*args, **kwargs)
        self.method = method
        self.existing_object = existing_object

    class Meta:
        skip_missing = True

    # def dump(self, *args, **kwargs):
    #     dumped, errors = super(SentiSchema, self).dump(*args, **kwargs)
    #     for field in dumped:
    #         metadata = self.declared_fields[field].metadata
    #         if metadata.get('load_only', False):
    #             del dumped[field]
    #
    #     return dumped, errors

    def make_object(self, data):
        load_data = {}
        for col in data:
            metadata = self.declared_fields[col].metadata
            if metadata.get('dump_only', False) or (
                    self.method == 'POST' and metadata.get('put_only', False)) or (
                        self.method == 'PUT' and metadata.get('post_only', False)):
                continue
            else:
                load_data[col] = data[col]
        if self.existing_object:
            for col in load_data:
                setattr(self.existing_object, col, load_data[col])
            return self.existing_object
        else:
            return self.opts.model(**load_data)