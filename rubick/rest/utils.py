import warnings

from flask.ext.restful.reqparse import Argument
from flask.ext.restful import wraps
from flask.ext.login import current_user
from marshmallow.compat import iteritems
from smore.swagger import field2property
from sqlalchemy import inspect
from flask.ext.restful import marshal, Resource, reqparse, fields
from flask import request, abort


class OperationArgument(Argument):

    def __init__(self, *args, **kwargs):
        ref_column = kwargs.pop('ref_column', args[0])
        operation = kwargs.pop('operation', 'eq')
        super(OperationArgument, self).__init__(*args, **kwargs)
        self.operation = operation
        self.ref_column = ref_column


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated():
            if len(current_user.roles):
                return func(*args, **kwargs)
            else:
                abort(401)
        else:
            abort(401)
    return wrapper


class SecureResource(Resource):

    method_decorators = [authenticate]


def list_marshal(data, marshal_fields, envelope=None):
    final_list = []
    for item in data:
        final_list.append(marshal(item, marshal_fields))
    if envelope:
        return {envelope: final_list}
    else:
        return final_list


def safe_bool(value):
    if value.lower() == 'false' or value == 0 or \
            value is False or value == 0.0:
        return False
    elif value.lower() == 'true' or value == 1 or \
            value is True or value == 1.0:
        return True
    else:
        abort(400, message='Boolean is required')
    return bool(value)


def get_parameters_from_parser(tag_parser, extra_dict=None):

    type_dict = {'str': ['', 'String'],
                 'validator': ['Relationship', 'DB Model'],
                 'property_list': ['Nested object', 'Name-Value pair'],
                 'datetime_str': ['', 'DateTime'], 'float': ['', 'float'],
                 'date_tuple': ['Nested list', 'Date time'],
                 'safe_bool': ['', 'boolean'], 'int': ['', 'Integer']}
    parameters = list()

    if extra_dict is not None:
        for new_dict in extra_dict:
            parameters.append(new_dict)

    for arg in tag_parser.args:
        if callable(arg.type) and arg.type.__name__ in type_dict:
            arg_type = type_dict[arg.type.__name__][1]
        else:
            arg_type = str(arg.type)

        if arg.name == 'properties':
            param_type = 'body'
        else:
            param_type = 'query'

        swagger_dict = {
            "name": str(arg.name), "description": str(arg.help),
            "required": arg.required, "dataType": arg_type,
            "paramType": param_type}
        parameters.append(swagger_dict)
    return parameters


base_get = reqparse.RequestParser()
base_get.add_argument('name', type=str)
base_get.add_argument('id', type=int)

base_put = reqparse.RequestParser(argument_class=OperationArgument)
base_put.add_argument('id', type=int, required=True)
base_put.add_argument('name', type=str)

base_post = reqparse.RequestParser(argument_class=OperationArgument)
base_post.add_argument('name', type=str, required=True)

base_delete = reqparse.RequestParser(argument_class=OperationArgument)
base_delete.add_argument('id', type=int, required=True)

base_fields = {
    'name': fields.String,
    'id': fields.Integer
}


class BaseControl(Resource):

    orm = None
    schema = None
    get_parser = base_get
    post_parser = base_post
    put_parser = base_put
    delete_parser = base_delete
    session = None

    def get_base_query(self, exclude=[]):
        args = self.get_parser.parse_args()

        query_filters = []
        for arg in self.get_parser.args:
            request_arg = args[arg.name]
            if request_arg:
                if arg.name not in exclude:
                    if isinstance(arg, OperationArgument):
                        column = getattr(self.orm, arg.ref_column)
                        if arg.operation == 'ilike':
                            filter = column.ilike('%' + request_arg + '%')
                        else:
                            filter = column == request_arg
                    else:
                        column = getattr(self.orm, arg.name)
                        filter = column == request_arg
                    query_filters.append(filter)

        base_query = self.orm.query.filter(*query_filters) \
            .order_by(self.orm.id)
        return base_query

    def get(self):

        instances = self.get_base_query().all()

        data, errors = self.schema().dump(instances, many=True)

        return data, 201

    def post(self):
        post_json = request.json
        # args = self.post_parser.parse_args()
        new_obj, errors = self.schema().load(post_json)
        if errors.keys():
            return errors, 400
        try:
            self.session.add(new_obj)
            self.session.commit()
        except Exception as e:
            errors['sql'] = e.message
            return errors, 400
        return self.schema(method='POST').dump(new_obj).data, 201


class BaseSingleControl(Resource):

    orm = None
    schema = None
    session = None

    def get_item(self, base_id):
        pk = inspect(self.orm).primary_key[0].name
        item = self.session.query(self.orm) \
            .filter(getattr(self.orm, pk) == base_id).first()
        return item

    def get(self, base_id):
        item = self.get_item(base_id)
        if item is None:
            return {'error': 'Resource not found'}, 404
        else:
            return self.schema().dump(item, many=False).data, 200

    def put(self, base_id):
        request_json = request.json
        # print('In the put: ', request_json)
        item = self.get_item(base_id)

        if item is None:
            return {'error': 'Resource not found'}, 404
        else:
            dumped = self.schema().dump(item).data
            dumped.update(request_json)
            instance, errors = self.schema(
                method='PUT', existing_object=item).load(dumped)
            # print('After load: ', instance)
            if instance.id != item.id:
                return {'error': 'Cannot change ID in put request'}, 400
            if len(errors.keys()):
                return errors, 400
            # else:
            #     for arg in request_json:
            #         print arg, request_json[arg]
            #         setattr(item, arg, request_json[arg])
            # for col in request_json:
            #     setattr(item, col, getattr(instance, col))
            self.session.merge(item)
            self.session.commit()
            new_instance = self.get_item(base_id)

        return self.schema().dump(new_instance, many=False).data, 201

    def delete(self, base_id):
        instance = self.get_item(base_id)
        self.session.delete(instance)
        self.session.commit()
        return {'success': 'Deleted type entry'}, 201


def schema2jsonschema(schema_cls, use_refs=True):
    """Return the JSON Schema Object for a given marshmallow
    :class:`Schema <marshmallow.Schema>`. Schema may optionally provide the ``title`` and
    ``description`` class Meta options.
    https://github.com/wordnik/swagger-spec/blob/master/versions/2.0.md#schemaObject
    Example: ::
        class UserSchema(Schema):
            _id = fields.Int()
            email = fields.Email(description='email address of the user')
            name = fields.Str()
            class Meta:
                title = 'User'
                description = 'A registered user'
        schema2jsonschema(UserSchema)
        # {
        #     'title': 'User', 'description': 'A registered user',
        #     'properties': {
        #         'name': {'required': False,
        #                 'description': '',
        #                 'type': 'string'},
        #         '_id': {'format': 'int32',
        #                 'required': False,
        #                 'description': '',
        #                 'type': 'integer'},
        #         'email': {'format': 'email',
        #                 'required': False,
        #                 'description': 'email address of the user',
        #                 'type': 'string'}
        #     }
        # }
    :param type schema_cls: A marshmallow :class:`Schema <marshmallow.Schema>`
    :rtype: dict, a JSON Schema Object
    """
    if getattr(schema_cls.Meta, 'fields', None) or getattr(schema_cls.Meta, 'additional', None):
        warnings.warn('Only explicitly-declared fields will be included in the Schema Object. '
                'Fields defined in Meta.fields or Meta.additional are excluded.')
    ret = {'properties': {}}
    # schema_inst = schema_cls()
    exclude = set(getattr(schema_cls.Meta, 'exclude', []))
    for field_name, field_obj in iteritems(schema_cls._declared_fields):
        if field_name in exclude:
            continue
        if field_obj.metadata.get('dump_only'):
            continue
        ret['properties'][field_name] = field2property(field_obj)
        if field_obj.required:
            ret.setdefault('required', []).append(field_name)
    if hasattr(schema_cls, 'Meta'):
        if hasattr(schema_cls.Meta, 'title'):
            ret['title'] = schema_cls.Meta.title
        if hasattr(schema_cls.Meta, 'description'):
            ret['description'] = schema_cls.Meta.description
    return ret


