from setuptools import setup, find_packages

from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='rubick',
    version='0.1.1',
    description='Sentilus Utilities',
    url='https://bitbucket.org/sentilus/rubick',
    author='Sentilus (Pty) Ltd.',
    author_email='info@sentilus.co.za',
    licence='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Licence :: OSI Approved :: MIT Licence',
        'Programming Language :: Python :: 2.7'
    ],
    packages=find_packages(),
    package_dir={'rubick': 'rubick'},
    include_package_data=True,
    install_requires=[
        'marshmallow==1.2.6',
        'smore',
        'sqlalchemy',
        'flask',
        'flask-restful',
        'flask-login',
        'flask-marshmallow'
    ])
